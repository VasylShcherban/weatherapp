package com.example.vasyl.weatherapp.service;

import android.os.AsyncTask;

import com.example.vasyl.weatherapp.service.model.WeatherData;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.lang.reflect.Modifier;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;



public class APIService {

    private static WeatherService weatherService;
    private static final String URL = "http://api.openweathermap.org/data/2.5";

    public interface WeatherService {

        public
        @GET("/data/2.5/forecast")
        Call<WeatherData> getForecast(@Query("q") String q,
                                      @Query("mode") String mode,
                                      @Query("units") String units,
                                      @Query("type") String type,
                                      @Query("lang") String lang,
                                      @Query("APPID") String appiId);
    }

    public static WeatherService getApiService(){
        Gson gson = new GsonBuilder()
                .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                .setDateFormat("yyyy-MM-dd HH:mm:ss")
                .create();

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .build();

        final String API_URL = "http://api.openweathermap.org";
        Retrofit restAdapter = new Retrofit.Builder()
                .baseUrl(API_URL)
                .callbackExecutor(AsyncTask.THREAD_POOL_EXECUTOR)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        weatherService = restAdapter.create(WeatherService.class);

        return weatherService;

    }
}
