package com.example.vasyl.weatherapp.ui;

import android.Manifest;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vasyl.weatherapp.service.APIService;
import com.example.vasyl.weatherapp.R;
import com.example.vasyl.weatherapp.service.model.WeatherData;
import com.example.vasyl.weatherapp.utils.GPSTracker;
import com.example.vasyl.weatherapp.utils.ImageUtils;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MainActivity extends AppCompatActivity {

    private static final int LOCATION_REQUEST_CODE = 11;
    private static final int LOCATION_REQUEST_MAP = 12;


    private final String format = "json";
    private final String units = "metric";
    private final String type = "hour";

    @BindString(R.string.API_ID)
    String API_ID;
    @BindString(R.string.lang)
    String lang;

    @BindView(R.id.weather_temperature)
    TextView weatherTemperature;

    @BindView(R.id.city)
    TextView weatherCity;

    @BindView(R.id.weather_description)
    TextView weatherDescription;

    @BindView(R.id.weather_icon)
    ImageView weatherIcon;

    @BindView(R.id.open_map_activity)
    FloatingActionButton openMap;

    @BindView(R.id.swipeRefresh)
    SwipeRefreshLayout swipeRefresh;

    private String locationQuery;
    private GPSTracker gps;
    private Geocoder geocoder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setTitle(R.string.app_name);

        locationQuery = "Lviv";
//        locationQuery = getCityName();

        if (ContextCompat.checkSelfPermission(this, Manifest.permission_group.LOCATION) == PackageManager.PERMISSION_GRANTED) {

        } else {
            String[] permissionRequested = {Manifest.permission_group.LOCATION};
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(permissionRequested, LOCATION_REQUEST_CODE);
            }
        }

        initUI();


    }

    @OnClick(R.id.open_map_activity)
    void openMapActivity() {
        Intent intent = new Intent(this, MapActivity.class);
        startActivityForResult(intent, LOCATION_REQUEST_MAP);
    }


    private void initUI() {
        swipeRefresh.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        getWeather(locationQuery);
                    }
                }
        );
        getWeather(locationQuery);
    }

    private void getWeather(String locationQuery) {


        Call<WeatherData> call = APIService.getApiService().getForecast(locationQuery, format, units, type, lang, API_ID);
        call.enqueue(new Callback<WeatherData>() {
            @Override
            public void onResponse(Call<WeatherData> call, Response<WeatherData> response) {
                if (response.isSuccessful()) {

                    final String cityName = response.body().getCity().getName();
                    final double temperature = response.body().getList().get(0).getMain().getTempMax();
                    int iconName = response.body().getList().get(0).getWeather().get(0).getId();
                    final int icon = ImageUtils.getIconResourceForWeatherCondition(iconName);
                    final String description = response.body().getList().get(0).getWeather().get(0).getDescription();

                    if (MainActivity.this != null) {
                        MainActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                swipeRefresh.setRefreshing(false);
                                weatherTemperature.setText(getResources().getString(R.string.temp_metrics, Math.round(temperature)));
                                weatherCity.setText(cityName);
                                Picasso.with(MainActivity.this).load(icon).into(weatherIcon);
                                weatherDescription.setText(description);

                            }
                        });
                    }

                } else {
                    swipeRefresh.setRefreshing(false);
                }
            }

            @Override
            public void onFailure(Call<WeatherData> call, Throwable t) {
                swipeRefresh.setRefreshing(false);
                Toast.makeText(MainActivity.this, R.string.error_download_data, Toast.LENGTH_LONG).show();
            }
        });

    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    public String getCityName() {
        String cityName = "null";
        gps = new GPSTracker(this);
        double latitude = 0.0;
        double longitude = 0.0;
        if (gps.canGetLocation()) {
            latitude = gps.getLatitude();
            longitude = gps.getLongitude();

            System.out.println("Lat: " + latitude);
            System.out.println("Lon: " + longitude);
            geocoder = new Geocoder(this, Locale.getDefault());
            List<Address> addresses = null;
            try {
                addresses = geocoder.getFromLocation(latitude, longitude, 1);
                cityName = addresses.get(0).getAddressLine(2);
            } catch (IOException e) {
                e.printStackTrace();
            }
            swipeRefresh.setRefreshing(false);

        } else {
            gps.showSettingsAlert();
        }

        return cityName;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == LOCATION_REQUEST_MAP) {
            if (resultCode == RESULT_OK) {
                String returnString = data.getStringExtra("cityName");
                getWeather(returnString);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case LOCATION_REQUEST_CODE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    initUI();
                } else {
                    swipeRefresh.setRefreshing(false);
                    Toast.makeText(this, "Unable invoke camera without pernision", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) searchItem.getActionView();

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        if(null!=searchManager ) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        }
        searchView.setIconifiedByDefault(true);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                getWeather(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        gps.stopUsingGPS();
    }
}
